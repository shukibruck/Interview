﻿$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/.*(\/|\\)/, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text');

        input.val(label);


    });

    $('.compareBtn').on('click',
        function () {
            compare();
        });

    var txtFromFile1;
    var txtFromFile2;

    var input = document.getElementById("file1");
    var input2 = document.getElementById("file2");

    input.addEventListener("change", function () {
        if (this.files && this.files[0]) {
            var myFile = this.files[0];
            var reader = new FileReader();

            reader.addEventListener('load', function (e) {
                txtFromFile1 = e.target.result;
                console.log(txtFromFile1);
            });

            reader.readAsBinaryString(myFile);
        }
    });

    input2.addEventListener("change", function () {
        if (this.files && this.files[0]) {
            var myFile = this.files[0];
            var reader = new FileReader();

            reader.addEventListener('load', function (e) {
                txtFromFile2 = e.target.result;
                console.log(txtFromFile2);
            });

            reader.readAsBinaryString(myFile);
        }
    });


    function compare() {
        // get the baseText and newText values from the two textboxes, and split them into lines
        var base = difflib.stringAsLines(txtFromFile1);
        var newtxt = difflib.stringAsLines(txtFromFile2);

        // create a SequenceMatcher instance that diffs the two sets of lines
        var sm = new difflib.SequenceMatcher(base, newtxt);

        // get the opcodes from the SequenceMatcher instance
        // opcodes is a list of 3-tuples describing what changes should be made to the base text
        // in order to yield the new text
        var opcodes = sm.get_opcodes();
        var diffoutputdiv = $("#diffoutput");
        while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
        var contextSize = $("#contextSize").value;
        contextSize = contextSize ? contextSize : null;

        // build the diff view and add it to the current DOM
        diffoutputdiv.append(diffview.buildView({
            baseTextLines: base,
            newTextLines: newtxt,
            opcodes: opcodes,
            // set the display titles for each resource
            baseTextName: "Base Text",
            newTextName: "New Text",
            contextSize: contextSize,
            viewType: $("inline").checked ? 1 : 0
        }));

        // scroll down to the diff view window.
    };
});