﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DifferMVC.Startup))]
namespace DifferMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
