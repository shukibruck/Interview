﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fibonacci
{
    public class Time
    {
        private static Timer _timer;
        private static readonly Stopwatch _stopWatch = Stopwatch.StartNew();
        private static int _period = 3000;

        public static async Task Run()
        {
            await Task.Run(
                () =>
                {
                    Scheduler();
                });
        }

        private static void ShowCurrentProccessTime(object o)
        {
            Console.WriteLine($"Current proccess time : {_stopWatch.Elapsed.TotalSeconds}");
        }

        private static void Scheduler()
        {
            _timer = new Timer(ShowCurrentProccessTime);
            _timer.Change(0, _period);
        }
    }
}
