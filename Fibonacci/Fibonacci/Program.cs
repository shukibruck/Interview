﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();

            Console.ReadKey();
        }

        static async Task MainAsync()
        {
            try
            {
                Time.Run();
            }
            catch (Exception exception)
            {
                throw;
            }

            Fibonacci.Start(50);

        }
    }
}
